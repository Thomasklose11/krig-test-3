﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krig
{
    public class Rules
    {
        // (Method) Sammenligner kort fra spillerne
        public static int Sammenligning(int[] spiller1, int[] spiller2)
        {
            //Opretter en int datatype til at returnere et resultat, hvor at 1=spiller1, 2=spiller2 og 3=Uafgjordt
            int resultat = 0;
            if (spiller1[0] > spiller2[0])
            {
                resultat = 1;
            }
            else if (spiller1[0] < spiller2[0])
            {
                resultat = 2;
            }
            else if(spiller1[0] == spiller2[0])
            {
                resultat = 3;
            }
            
            return resultat;
        }
        // (Method, int datatype) 
        public static int Krig(int[] spiller1, int[] spiller2)
        {
            int temp = 0;
            while (spiller1[0 + temp] == spiller2[0 + temp])
            {
                temp += 3;
                Console.WriteLine("WAR!!!");
                Console.WriteLine("Both player put 2 card face down. ");
                Console.Write("Press \"ENTER\" to continue");
                Console.ReadLine();
                Console.WriteLine("Player 1 drew: {0}", spiller1[temp]);
                Console.WriteLine("Player 2 drew: {0}", spiller2[temp]);
                if (spiller1[temp] > spiller2[temp])
                {
                    Console.WriteLine("Player 1 wins");
                    break;
                }
                if (spiller1[temp] < spiller2[temp])
                {
                    Console.WriteLine("Player 2 wins");
                    break;
                }
            }
            return 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krig
{
    public class Deck
    {
        public static int [] Bland()
        {
            //Opretter array af datatypen int (Indenholder alle kort)
            int[] Kort = {
             2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
             2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
             2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
             2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};
            //Opretter objektet Random()
            Random kort = new Random();
            //Loop som blander kort
            for (int i = 0; i < Kort.Length; i++)
            {
                int j = kort.Next(i);
                int k = Kort[j];
                Kort[j] = Kort[i];
                Kort[i] = k;

            }
            //Returnerer blandede kort
            return Kort;
        }
        //Opretter en int metode til opdeling af kort
        public static int[] DeleKort1(int[] Kort)

        {
            //Opretter en arrays af int datatypen, denne indeholder 52 pladser (Til kort)
            int[] player1 = new int[52];
            //Loop som tager fra kort inputtet, heraf giver den kortd til spilleren
            for (int i = 0; i < Kort.Length; i++)
            {
                // (Condition) Giver de første 26 kort (kort.Length = 52, 52/2=26)
                if (i < Kort.Length / 2)
                {
                    player1[i] = Kort[i];
                }
            }
            return player1;
        }
        //Opretter en int metode til opdeling af kort
        public static int[] DeleKort2(int[] Kort)

        {
            //Opretter en arrays af int datatypen, denne indeholder 52 pladser (Til kort)
            int[] player2 = new int[52];

            //Loop som tager fra kort inputtet, heraf giver den kortd til spilleren
            for (int i = 0; i < Kort.Length; i++)
            {
                // (Condition) Giver de sidste 26 kort (kort.Length = 52, 52/2=26)
                if (i >= Kort.Length / 2)
                {
                    player2[i - 26] = Kort[i];
                }
            }
            return player2;
        }
    }

}


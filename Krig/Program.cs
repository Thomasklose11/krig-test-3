﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krig
{
    public class Program
    {
        static void Main(string[] args)
        {
            /*
            God ferie, skriv endelig kommentar her hvis der er noget som i ønsker ændret.
            Evt. kodelinje og klasse.

             */
            //Bland kort
            int[] deckTotal = Deck.Bland();
            //Opretter datatypen int til senere brug
            int temp = 0;
            //Kalder en funktion i klassen Deck
            int[] player1 = Deck.DeleKort1(deckTotal);
            //Kalder en funktion i klassen Deck
            int[] player2 = Deck.DeleKort2(deckTotal);

            //Outputter en linje med tekst
            Console.WriteLine("-----Player 1-----");
            //Loop til test (Viser alle kort)
            for (int i = 0; i < player1.Length; i++)
            {
                if(player1[i] != 0)
                    Console.WriteLine("Card {0}: {1}", i+1, player1[i]);
            }
            //Outputter en linje med tekst
            Console.WriteLine("-----Player 2-----");
            //Loop til test (Viser alle kort)
            for (int i = 0; i < player1.Length; i++)
            {
                if (player2[i] != 0)
                    Console.WriteLine("Card {0}: {1}", i + 1, player2[i]);
            }
            //(Condition) Tjekker om kort ikke er 0 (hvis det første kort i bunken er 0, er der ingen kort tilbage i dækket)
            if (player1[0] != 0 && player2[0] != 0)
            {
                Console.Write("press \"ENTER\" to continue");
                Console.ReadLine();
                //Outputter spillernes kort.
                Console.WriteLine("Player 1 drew: {0}", player1[0]); //Tuborg klammerne benytter overloads efter komma så {0} er første instans efter komma
                Console.WriteLine("Player 2 drew: {0}", player2[0]);
            }
            //Kalder funktion i Rules
            int sam = Rules.Sammenligning(player1, player2);
            //(Condition) tjekker om output fra klassen 'Rules' ikke er 3
            if (sam != 3)
                Console.WriteLine("Player {0} wins!!", sam.ToString());
            else // (Condition) Kalder funktion i klassen Krig hvis kort er ens.
                Rules.Krig(player1, player2);
            Console.ReadLine();
        }

    }
}
